// main script_list 화자변경
document.querySelectorAll('#script_list .conversation button').forEach(select_character => {
  select_character.addEventListener('click', function () {
    document.querySelectorAll('.active').forEach(active_list => {
      active_list.classList.remove('active');
    })
    document.querySelectorAll('.modal').forEach(open_modals => {
      open_modals.style.display = 'none';
    })
    document.querySelector('#character_list ul li a').classList.add('active');
    document.getElementById('speaker_change').style.display = 'block';
  })
});

// main character_list 캐릭터변경
document.querySelectorAll('#character_list ul li a').forEach(select_character => {
  select_character.addEventListener('click', function () {
    document.querySelectorAll('.active').forEach(active_list => {
      active_list.classList.remove('active');
    })
    document.querySelectorAll('.modal').forEach(open_modals => {
      open_modals.style.display = 'none';
    })
    document.querySelector('#character_list ul li a').classList.add('active');
    document.getElementById('character_change').style.display = 'block';
  })
});

// modal 배경 눌렀을때 끄기
document.querySelectorAll('.modal_bg').forEach(modal_bg => {
  modal_bg.addEventListener('click', () => {
    document.querySelectorAll('.active').forEach(active_list => {
      active_list.classList.remove('active');
    })
    document.querySelectorAll('.modal').forEach(open_modals => {
      open_modals.style.display = 'none';
    })
  })
})

// modal close icon 눌렀을때 끄기
document.querySelectorAll('.modal_close').forEach(modal_close => {
  modal_close.addEventListener('click', () => {
    document.querySelectorAll('.active').forEach(active_list => {
      active_list.classList.remove('active');
    })
    document.querySelectorAll('.modal').forEach(open_modals => {
      open_modals.style.display = 'none';
    })
  })
})

// all nav
document.querySelectorAll('.my_info ul li a')[0].addEventListener('click', function () {
  document.querySelectorAll('.active').forEach(active_list => {
    active_list.classList.remove('active');
  })
  document.querySelectorAll('.modal').forEach(open_modals => {
    open_modals.style.display = 'none';
  })
  document.querySelectorAll('.my_info ul li a')[0].classList.add('active');
  document.getElementById('personal_info_modify').style.display = 'block';
});

document.querySelectorAll('.my_info ul li a')[1].addEventListener('click', function () {
  document.querySelectorAll('.active').forEach(active_list => {
    active_list.classList.remove('active');
  })
  document.querySelectorAll('.modal').forEach(open_modals => {
    open_modals.style.display = 'none';
  })
  document.querySelectorAll('.my_info ul li a')[1].classList.add('active');
  document.getElementById('password_modify').style.display = 'block';
});

document.querySelectorAll('.my_info ul li a')[2].addEventListener('click', function () {
  document.querySelectorAll('.active').forEach(active_list => {
    active_list.classList.remove('active');
  })
  document.querySelectorAll('.modal').forEach(open_modals => {
    open_modals.style.display = 'none';
  })
  document.querySelectorAll('.my_info ul li a')[2].classList.add('active');
  document.getElementById('logout').style.display = 'block';
});

document.querySelectorAll('.function_list ul li a')[0].addEventListener('click', function () {
  document.querySelectorAll('.active').forEach(active_list => {
    active_list.classList.remove('active');
  })
  document.querySelectorAll('.modal').forEach(open_modals => {
    open_modals.style.display = 'none';
  })
  document.querySelectorAll('.function_list ul li a')[0].classList.add('active');
  document.getElementById('new_file').style.display = 'block';
});

document.querySelectorAll('.function_list ul li a')[1].addEventListener('click', () => {
  document.querySelectorAll('.active').forEach(active_list => {
    active_list.classList.remove('active');
  })
  document.querySelectorAll('.modal').forEach(open_modals => {
    open_modals.style.display = 'none';
  })
  document.querySelectorAll('.function_list ul li a')[1].classList.add('active');
  document.getElementById('import').style.display = 'block';
});

document.querySelectorAll('.function_list ul li a')[2].addEventListener('click', () => {
  document.querySelectorAll('.active').forEach(active_list => {
    active_list.classList.remove('active');
  })
  document.querySelectorAll('.modal').forEach(open_modals => {
    open_modals.style.display = 'none';
  })
  document.querySelectorAll('.function_list ul li a')[2].classList.add('active');
  document.getElementById('save').style.display = 'block';
});

document.querySelectorAll('.function_list ul li a')[3].addEventListener('click', () => {
  document.querySelectorAll('.active').forEach(active_list => {
    active_list.classList.remove('active');
  })
  document.querySelectorAll('.modal').forEach(open_modals => {
    open_modals.style.display = 'none';
  })
  document.querySelectorAll('.function_list ul li a')[3].classList.add('active');
  document.getElementById('new_name_save').style.display = 'block';
});

document.querySelectorAll('.function_list ul li a')[4].addEventListener('click', () => {
  document.querySelectorAll('.active').forEach(active_list => {
    active_list.classList.remove('active');
  })
  document.querySelectorAll('.modal').forEach(open_modals => {
    open_modals.style.display = 'none';
  })
  document.querySelectorAll('.function_list ul li a')[4].classList.add('active');
  document.getElementById('all_download').style.display = 'block';
});

document.querySelectorAll('.function_list ul li a')[5].addEventListener('click', () => {
  document.querySelectorAll('.active').forEach(active_list => {
    active_list.classList.remove('active');
  })
  document.querySelectorAll('.modal').forEach(open_modals => {
    open_modals.style.display = 'none';
  })
  document.querySelectorAll('.function_list ul li a')[5].classList.add('active');
  document.getElementById('all_delete').style.display = 'block';
});